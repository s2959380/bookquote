package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getBookPrice(String isbn){

        HashMap<String, Double> priceMap;

        priceMap = new HashMap<>();

        priceMap.put("1", 10.0);
        priceMap.put("2", 45.0);
        priceMap.put("3", 20.0);
        priceMap.put("4", 35.0);
        priceMap.put("5", 50.0);

        priceMap.put("default", 0.0);


        return priceMap.getOrDefault(isbn, priceMap.get("default"));
    }
}